# timeline-cleaner
Cleans out your timeline, only your twitter timeline though.

## Features:
- An easy and straightforward CLI!
- Supports wiping all of your current tweets
- Wipe away tweets based on date range
- Wipe tweets based on `archive.json` (wipe tweets pre-api changes)
- Unlike all liked content
- `equalizer` command to make it so you only follow mutuals!

## Install
### Pre-reqs:
1. Python (Windows: `https://www.python.org/downloads/`, Linux/Unix/BSD: Consult your package manager)
2. insure `pyenv` is able to be used
3. Git

### Install:
1. Clone the code: `git clone https://gitlab.com/cmhedrick/timeline-cleaner.git`
2. activate your virtual environment: `source env/bin/activate`
3. install the dependencies: `pip install -r req.txt`
4. You're done!

## Running the CLI:
1. activate virtual environment
2. run: `python timeline_cleaner.py`
3. You should be dropped into the prompt.

## Commands:
- `q` = `quit`: Will quit out of the CLI
- `w` = `wipe` timeline: Will begin to wipe away your entire timeline, removing all your tweets. Will pause occasionally when API limit is hit. *(can take awhile depending on number of tweets.)*
- `d` = wipe by `date` range: Will prompt you for a start date THEN end date. Will then remove tweets based on date range.
- `a` = wipe by `archive.json`: Will remove tweets based on `archive.json`. Requires you to create a directory names `tweetdir` in the project directory. Then you must rename `archive.json` to `tweet.json`. This must be done before running the command. The `archive.json` can be retrieved from your settings on twitter.
- `u` = `unlike` everything: Will unlike all of the tweets you liked.
- `e` = "`equalizer`": will begin unfollowing anyone who is not already following you back. Could help with focusing a users interactions.

